package wsdlToJava.CXF.Java14;

import java.io.IOException;

public class WsdlToJavaApplication {

	private static final String PATH_TO_GRADLE_PROJECT = "./";
    private static final String GRADLEW_EXECUTABLE = "gradlew.bat";
    private static final String BALNK = " ";
    private static final String GRADLE_TASK = "generateJavaClasses";
    
	public static void main(String[] args) {
		taskMethod();
	}

	public static boolean taskMethod() {
		String command = PATH_TO_GRADLE_PROJECT + GRADLEW_EXECUTABLE + BALNK + GRADLE_TASK;
		try	{
            Runtime.getRuntime().exec(command);
            return true;
        }
        catch (IOException e)	{
        	return false;
        }
	}
}
